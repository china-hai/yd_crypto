
/* Apache License 2.0 */

/*
	文件：yd_aes_cbc_cs.h
	作者：wzh
	邮箱：wangzhihai_138@163.com
	简介：AES算法模式CBC-CS三种变体实现，详情参考《SP800-38A的附件》
	版本：README.md定义
*/

#ifndef YD_AES_CBC_CS_H
#define YD_AES_CBC_CS_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>


void yd_aes_cbc_cs1_encrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);
void yd_aes_cbc_cs2_encrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);
void yd_aes_cbc_cs3_encrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);

void yd_aes_cbc_cs1_decrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);
void yd_aes_cbc_cs2_decrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);
void yd_aes_cbc_cs3_decrypt(uint8_t *in,
							uint8_t *out,
							uint8_t *key,
							uint8_t *iv,
							uint32_t length);

#ifdef __cplusplus
}
#endif

#endif /* YD_AES_CBC_CS_H */
